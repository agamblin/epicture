import React from 'react'
import {View, StyleSheet, FlatList, ActivityIndicator} from 'react-native'
import  retrieveItem  from '../API/Token/retrieveToken'
import {fetchFavoris} from "../API/ImgurApi";
import ImageItem from "./ImageItem";

class Favorites extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            images: [],
            isLoading: false
        }
    }

    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size={'large'} />
                </View>
            )
        }
    }

    _fetchFavorites() {
        this.setState({ isLoading: true });
        fetchFavoris(this.state.accessToken).then((data) => {
            console.log(data.data);
                this.setState({
                    images: data.data,
                    isLoading: false
                });
            }
        );
    }

    componentDidMount() {
        retrieveItem('ACCESS_TOKEN').then((token) => {
            this.setState({
                accessToken: token
            });
            this._fetchFavorites();
        }).catch((error) => {
            console.log('Promise is rejected with error: ' + error);
        });
    }

    render() {
        return (
            <View style={styles.main_container}>
                <View styles={styles.content_container}>
                    <FlatList
                        data={this.state.images}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={({item}) => <ImageItem image={item}/>}
                        onEndReachedThreshold={0.5}
                        onEndReached={() => {
                            this._fetchFavorites();
                        }}
                    />
                </View>
                {this._displayLoading()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: 'white'
    },
    header_container: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    content_container: {
        flex: 1
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default Favorites