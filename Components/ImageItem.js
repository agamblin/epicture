import React from 'react'
import {View, Image, Text, StyleSheet, TouchableWithoutFeedback, Alert } from 'react-native'
import { fetchImage } from "../API/ImgurApi"
import { Rating } from 'react-native-elements'
import retrieveItem from "../API/Token/retrieveToken"
import { favoriteImage } from "../API/ImgurApi";


class   ImageItem   extends React.Component {
    lastTap = null;

    constructor(props) {
        super(props);
        this.state = {
            accessToken: ''
        }
    }

    _toggleLike() {
        let id = this.props.image.cover !== undefined ? this.props.image.cover : this.props.image.id;
        favoriteImage(this.state.accessToken, id);
        Alert.alert("Added " + this.props.image.title + " to favorites.");
    }

    _handleDoubleTap = () => {
        const now = Date.now();
        const DOUBLE_PRESS_DELAY = 300;
        if (this.lastTap && (now - this.lastTap) < DOUBLE_PRESS_DELAY) {
            this._toggleLike();
        } else {
            this.lastTap = now;
        }
    };

   static _getRating(ups, downs) {
        let totalCount = ups + downs;
        return (ups * 5) / totalCount;
    }

    _getRightPath() {
        if (this.props.image.cover !== undefined)
            return this.props.image.cover;
        else
            return this.props.image.id;
    }

    componentDidMount() {
        retrieveItem('ACCESS_TOKEN').then((token) => {
            this.setState({
                accessToken: token
            });
        }).catch((error) => {
            console.log('Promise is rejected with error: ' + error);
        });
    }

    render() {
        const image = this.props.image;
        return (
            <View style={styles.main_container}>
                <View style={styles.header_container}>
                    <Text style={styles.title_text}>{image.title}</Text>
                </View>
                <TouchableWithoutFeedback onPress={() => this._handleDoubleTap()}>
                    <Image
                        style={styles.image}
                        source={{uri: fetchImage(this._getRightPath())}}
                    />
                </TouchableWithoutFeedback>
                <View style={styles.content_container}>
                    <View style={styles.rating_container}>
                        <Rating
                            type="heart"
                            imageSize={20}
                            readonly
                            startingValue={ImageItem._getRating(image.ups, image.downs)}
                        />
                    </View>
                    <View style={styles.description_container}>
                        <Text style={styles.description_text} numberOfLines={6}>{image.description}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flexDirection: 'column',
        alignItems: 'center',
        borderWidth: 0.5,
        borderColor: 'black',
    },
    image: {
        height: 320,
        width: 320,
        flex: 1,
        margin: 5,
        backgroundColor: 'gray'
    },
    content_container: {
        flexDirection: 'row',
        margin: 5
    },
    header_container: {
       margin: 10
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 15,
        flexWrap: 'wrap',
        paddingRight: 5
    },
    rating_container: {
        alignItems: 'center',
        flex: 1
    },
    description_container: {
        margin: 10,
        flex: 1
    },
    description_text: {
        fontStyle: 'italic',
        color: 'black'
    },
    date_container: {
    },
    date_text: {
        textAlign: 'right',
        fontSize: 14
    }
});

export default ImageItem;