import React from 'react'
import retrieveItem from '../API/Token/retrieveToken'
import BottomNavigation from '../Navigation/NavigationBottom'

class Index  extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            accessToken: ''
        }
    }

    componentDidMount() {
        retrieveItem('ACCESS_TOKEN').then((token) => {
            this.setState({
                accessToken: token
            });
        }).catch((error) => {
            console.log('Promise is rejected with error: ' + error);
        });
    }

    render() {
        return (
            <BottomNavigation/>
        )
    }
}

export default Index