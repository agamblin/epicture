import React from 'react'
import {View, StyleSheet, FlatList, ActivityIndicator} from 'react-native'
import { Icon } from 'react-native-elements'
import  retrieveItem  from '../API/Token/retrieveToken'
import { fetchUserProfile } from "../API/ImgurApi";
import ImageItem from "./ImageItem";

class MyImages extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            images: [],
            isLoading: false
        }
    }

    _fetchUsrProfile() {
        this.setState({ isLoading: true });
        fetchUserProfile(this.state.accessToken, 'me').then((data) => {
                this.setState({
                    images: data.data,
                    isLoading: false
                });
            }
        );
    }

    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size={'large'} />
                </View>
            )
        }
    }

    _refresh() {
        this.setState({ images: [] });
        this._fetchUsrProfile();
    }

    componentDidMount() {
        retrieveItem('ACCESS_TOKEN').then((token) => {
            this.setState({
                accessToken: token
            });
            this._fetchUsrProfile();
        }).catch((error) => {
            console.log('Promise is rejected with error: ' + error);
        });
    }

    render() {
        return (
            <View style={styles.main_container}>
                <Icon name="refresh-cw" type="feather" size={10} onPress={() => this._refresh()}/>
                <View styles={styles.content_container}>
                    <FlatList
                        data={this.state.images}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={({item}) => <ImageItem image={item}/>}
                    />
                </View>
                {this._displayLoading()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: 'white'
    },
    header_container: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    content_container: {
        flex: 1
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default MyImages