import React from 'react'
import {View, StyleSheet,  ActivityIndicator} from 'react-native'
import ImagePicker from 'react-native-image-picker'
import RNFetchBlob from 'rn-fetch-blob'
import retrieveItem from "../API/Token/retrieveToken";

const options = {
    title: 'Select a photo',
    takePhotoButtonTitle: 'Take a photo',
    chooseFromLibraryButtonTitle: 'Choose from gallery',
    quality: 1

};

class UploadImage extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            imageSource: null,
            accessToken: '',
            data: null,
            isLoading: false
        }
    }

    _uploadPhoto() {
        this.setState({
            isLoading: true
        });
        RNFetchBlob.fetch('POST', 'https://api.imgur.com/3/image', {
            Authorization : `Bearer ${this.state.accessToken}`,
            'Content-Type' : 'multipart/form-data',
        }, [
            { name : 'image', fileName: 'image.png', type:'image/png', data: this.state.data},
        ]).then((resp) => {
            this.setState({
                isLoading: false
            });
            this.props.navigation.navigate("upload");
        }).catch((err) => {
            // ...
        })
    }
    _pickPhoto() {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                this.setState({
                    imageSource: null
                });
                this.props.navigation.navigate("upload");
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const source = { uri: response.uri };
                this.setState({
                    imageSource: source,
                    data: response.data
                });
                this._uploadPhoto()
            }
        });
    }

    componentDidMount() {
        retrieveItem('ACCESS_TOKEN').then((token) => {
            this.setState({
                accessToken: token
            });
            this._pickPhoto()
        }).catch((error) => {
            console.log('Promise is rejected with error: ' + error);
        });
    }

    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size={'large'} />
                </View>
            )
        }
    }

    render() {
        return (
            <View style={styles.main_container}>
                {this._displayLoading()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignContent: 'center'
    },
    button: {
        height: 50,
        width: 250,
        backgroundColor: 'black'
    },
    text_button: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center'
    },
    image: {
        width: 200,
        height: 200
    }
});

export default UploadImage