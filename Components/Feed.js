import React from 'react';
import {View, ActivityIndicator, StyleSheet, FlatList} from 'react-native';
import { ButtonGroup } from 'react-native-elements';
import {fetchFeed} from "../API/ImgurApi";
import ImageItem from './ImageItem';
import  retrieveItem  from '../API/Token/retrieveToken'

class Feed  extends React.Component {

    constructor(props) {
        super(props);
        this.page = 0;
        this.period = "week";
        this.state = {
            images: [],
            isLoading: false,
            accessToken: ''
        }
    }

    _updateIndex = (index) => {
        if (index === 0) {
            this.period = "day";
        } else if (index === 1) {
            this.period = "week";
        } else if (index === 2) {
            this.period = "month";
        } else if (index === 3) {
            this.period = "year";
        } else if (index === 4) {
            this.period = "all";
        }
        this._newFetch();
    };

    _newFetch() {
        this.page = 0;
        this.setState({
            images: []
        }, () => {
            this._fetchFeed();
        });
    }

    _fetchFeed() {
        this.setState({ isLoading: true });
        fetchFeed(this.state.accessToken, this.period, this.page + 1).then((data) => {
            this.page = this.page + 1;
            this.setState({
                    images: [ ...this.state.images, ...data.data ],
                    isLoading: false
                });
            }
        );
    }

    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size={'large'} />
                </View>
            )
        }
    }

    componentDidMount() {
        retrieveItem('ACCESS_TOKEN').then((token) => {
            this.setState({
                accessToken: token
            });
            this._fetchFeed()
        }).catch((error) => {
            console.log('Promise is rejected with error: ' + error);
        });
    }

    render() {
        return (
            <View style={styles.main_container}>
                <View style={styles.button_container}>
                    <ButtonGroup
                        selectedBackgroundColor="pink"
                        onPress={this._updateIndex}
                        selectedIndex={this.state.index}
                        buttons={['Day', 'Week', 'Month', 'Year', 'All']}
                        containerStyle={{height: 30}} />
                </View>
                <View style={styles.result_container}>
                    <FlatList
                        data={this.state.images}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={({item}) => <ImageItem image={item}/>}
                        onEndReachedThreshold={0.5}
                        onEndReached={() => {
                            if (this.state.images.length > 0) {
                                this._fetchFeed();
                            }
                        }}
                    />
                </View>
                {this._displayLoading()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    button_container: {
    },
    result_container: {
        flex: 1,
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default Feed;