import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Icon, Text } from 'react-native-elements'

class   Login extends React.Component {

    _displayWebView() {
        this.props.navigation.navigate("WebViewImgur");
    }

    render() {
        return (
            <View style={styles.main_container}>
                <View style={styles.container_icon}>
                    <Icon
                        name='login'
                        type='entypo'
                        color='black'
                        size={50}
                        iconStyle={styles.login_icon}
                        onPress={() => this._displayWebView()}
                    />
                </View>
                <Text h5>Tap to login</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        flexDirection:'column',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    container_icon: {
        height: 50,
        width : 50
    },
    login_icon: {
    }
});

export default Login