import React from 'react'
import {AsyncStorage, StyleSheet, WebView} from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation';
import storeItem from '../../API/Token/storeToken'

const webviewRef = 'webview';
const CLIENT_ID = 'bbc4e4148bb5e23';


class LoginImgur extends React.Component {
    accessToken : string;
    expiresIn : string;
    refreshToken : string;
    userName : string;

    constructor(props) {
        super(props);
    }

    _resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Index' })],
    });

    _changeNavigationState = async (webView) => {
       if (this._splitUrl(webView.url) === true) {
           storeItem('ACCESS_TOKEN', this.accessToken);
           storeItem('REFRESH_TOKEN', this.refreshToken);
           storeItem('EXPIRES_IN', this.expiresIn);
           storeItem('USERNAME', this.userName);
           this.props.navigation.dispatch(this._resetAction);
       }
    };

    _splitUrl(url : String) {
        if (url.search("access_token=") > 0) {
            let array = url.split("=");
                this.accessToken =   array[2].split('&')[0];
                this.expiresIn = array[3].split('&')[0];
                this.refreshToken =  array[5].split('&')[0];
                this.userName =  array[6].split('&')[0];
            return (true);
        }
        return (false);
    }



    webviewProps = {
        style: styles.webview_container,
        ref: webviewRef,
        javaScriptEnabled: true,
        onNavigationStateChange: this._changeNavigationState.bind(this),
        source: {
            uri: 'https://api.imgur.com/oauth2/authorize?client_id=' + CLIENT_ID + '&response_type=token&state=APPLICATION_STATE',
        }
    };

    render() {
        return (
            <WebView  {...this.webviewProps}/>
        )

    }
}


const styles = StyleSheet.create({
    main_container: {
        backgroundColor: 'black'
    },
    webview_container: {
        flex: 1
    }
});

export default LoginImgur