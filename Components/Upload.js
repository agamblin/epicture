import React from 'react'
import { View, StyleSheet} from 'react-native'
import { Icon } from 'react-native-elements'


class Upload extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            accessToken: ''
        }
    }

    componentDidMount() {
    }

    _uploadImage() {
        this.props.navigation.navigate("uploadImage");
    }

    render() {
        console.log(this.props.navigation);
        return (
            <View style={styles.main_container}>
                    <Icon  name='cloud-upload' type='font-awesome' size={40} onPress={() => this._uploadImage()}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignContent: 'center'
    }
});

export default Upload