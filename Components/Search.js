import React from 'react'
import { View, TextInput, ActivityIndicator, StyleSheet, Button, FlatList } from 'react-native'
import { fetchImgFromString } from "../API/ImgurApi";
import retrieveItem from "../API/Token/retrieveToken";
import ImageItem from "./ImageItem";

class Search extends React.Component {

    constructor(props) {
        super(props);
        this.searchedText = "";
        this.page = 0;
        this.state = {
            images: [],
            accessToken: '',
            isLoading: false
        }
    }

    _loadImages() {
        if (this.searchedText.length > 0) {
            console.log(this.state.accessToken);
            this.setState({ isLoading: true });
            fetchImgFromString(this.state.accessToken, this.searchedText, this.page+1).then(data => {
                console.log(data);
                this.page = this.page + 1;
                this.setState({
                    images: [ ...this.state.images, ...data.data ],
                    isLoading: false
                })
            })
        }
    }

    _searchTextInputChanged(text) {
        this.searchedText = text
    }

    _searchImages() {
        this.page = 0;
        this.setState({
            images: [],
        }, () => {
            this._loadImages();
        })
    }

    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    componentDidMount() {
        retrieveItem('ACCESS_TOKEN').then((token) => {
            this.setState({
                accessToken: token
            });
        }).catch((error) => {
            console.log('Promise is rejected with error: ' + error);
        });
    }

    render() {
        return (
            <View style={styles.main_container}>
                <TextInput
                    style={styles.textinput}
                    placeholder='Category (Cat, dog, pizza...)'
                    onChangeText={(text) => this._searchTextInputChanged(text)}
                    onSubmitEditing={() => this._searchImages()}
                />
                <FlatList
                    data={this.state.images}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({item}) => <ImageItem image={item}/>}
                    onEndReachedThreshold={0.5}
                    onEndReached={() => {
                        if (this.state.images.length > 0) {
                            this._loadImages()
                        }
                    }}
                />
                {this._displayLoading()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        marginTop: 20
    },
    textinput: {
        marginLeft: 5,
        marginRight: 5,
        height: 50,
        borderColor: '#000000',
        borderWidth: 1,
        paddingLeft: 5
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default Search