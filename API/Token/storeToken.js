import {AsyncStorage} from "react-native";

export default storeItem = async (key, item) => {
    try {
        let jsonOfItem = await AsyncStorage.setItem(key, JSON.stringify(item));
        return jsonOfItem;
    } catch (error) {
        console.log(error.message);
    }
};