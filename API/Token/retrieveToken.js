import {AsyncStorage} from "react-native";

export default retrieveItem = async (key) => {
    try {
        const retrieveItem =  await AsyncStorage.getItem(key);
        const item = JSON.parse(retrieveItem);
        return item;
    } catch (error) {
        console.log(error.message);
    }
};