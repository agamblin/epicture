import retrieveItem from "./Token/retrieveToken";

// retrieveItem('ACCESS_TOKEN').then((token) => {
//     this.setState({
//         accessToken: token
//     });
// }).catch((error) => {
//     console.log('Promise is rejected with error: ' + error);
// });

export function fetchUserProfile(token, usr) {
            const url = 'https://api.imgur.com/3/account/' + usr + '/images';
            return fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }).then((response) => response.json())
                .catch((error) => console.error(error));
}

export function fetchUserAvatar(token, usr) {
    const url = 'https://api.imgur.com/3/account/' + usr + '/avatar';
    return fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then((response) => response.json())
        .catch((error) => console.error(error));
}

export function fetchAccountSettings(token) {
    const url = 'https://api.imgur.com/3/account/' + me + '/settings';
    return fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then((response) => response.json())
        .catch((error) => console.error(error));
}

export function fetchAccountGalleryProfile(token, usr) {
    const url = 'https://api.imgur.com/3/account/' + usr + '/settings';
    return fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then((response) => response.json())
        .catch((error) => console.error(error));
}

export function fetchAlbumFromUser(token, usr, page) {
    const url = 'https://api.imgur.com/3/account/' + usr + '/albums/' + page;
    return fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then((response) => response.json())
        .catch((error) => console.error(error));
}

export function fetchAlbum(token, usr, albumHash) {
    const url = 'https://api.imgur.com/3/account/' + usr + '/album/' + albumHash;
    return fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then((response) => response.json())
        .catch((error) => console.error(error));
}

export function fetchFeed(token, period, page) {
    const url = 'https://api.imgur.com/3/gallery/top/top/' + period + '/' + page + '?showViral=true}&mature=true&album_previews=true';
    return fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then((response) => response.json())
        .catch((error) => console.error(error));
}

export function fetchFavoris(token) {
    const url = 'https://api.imgur.com/3/account/me/favorites/';
    return fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then((response) => response.json())
        .catch((error) => console.error(error));
}

export function favoriteImage(token, id) {
    const url = 'https://api.imgur.com/3/image/' + id + '/favorite';
    console.log(id);
    return fetch(url, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then((response) => response.json())
        .catch((error) => console.error(error));
}

export function fetchImage(id) {
    return 'http://i.imgur.com/' + id + 'm.jpg';
}

export function fetchImgFromString(token, searchString, page) {
    console.log("searching for " + searchString + " page " + page);
    const url = 'https://api.imgur.com/3/gallery/search/time/all/' + page + '?q=' + searchString;
    return fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then((response) => response.json())
        .catch((error) => console.error(error));

}