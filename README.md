# Pré-requis

- Node
- react-native

# Installation

type:
```bash
npm install
```

launch the following command
```bash
npm start
```
in a terminal and let it run

Now you can type:
```bash
react-native run-ios
```
For IOS

```bash
react-native run-android
```
for Android

Their is maybe some issues with the iOS build to some incompatibility with librairies using native elements.
I'll fix them ASAP.