import {createStackNavigator} from "react-navigation";
import  MyImages from '../../Components/MyImages'

const myImagesStackNavigator = createStackNavigator({
    myImages: {
        screen: MyImages,
        navigationOptions: {
            title: 'My images'
        }
    }
});

export default myImagesStackNavigator