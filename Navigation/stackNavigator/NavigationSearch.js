import {createStackNavigator} from "react-navigation";
import Search from "../../Components/Search";
import ImageDetail from "../../Components/ImageDetail";

const searchStackNavigator = createStackNavigator({
    Search: {
        screen: Search,
        navigationOptions: {
            title: 'Search'
        }
    },
    SearchDetail: {
        screen: ImageDetail
    }
});

export default searchStackNavigator