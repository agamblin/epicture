import {createStackNavigator} from "react-navigation";
import Feed from "../../Components/Feed";

const FeedStackNavigator = createStackNavigator({
    Feed: {
        screen: Feed,
        navigationOptions: {
            title: 'Feed'
        }
    }
});

export default FeedStackNavigator