import {createStackNavigator} from "react-navigation";
import  Favorites from '../../Components/Favorites'

const favorisStackNavigator = createStackNavigator({
    Favoris: {
        screen: Favorites,
        navigationOptions: {
            title: 'Favoris'
        }
    }
});

export default favorisStackNavigator