import { createStackNavigator } from 'react-navigation'
import Login from '../../Components/Login/Login'
import LoginImgur from '../../Components/Login/LoginImgur'
import Index from "../../Components/Index";
import { Icon } from 'react-native-elements'

const LoginStackNavigator = createStackNavigator({
    LoginPage: {
        screen: Login,
        navigationOptions: {
            title: 'Login'
        }
    },
    WebViewImgur: {
        screen: LoginImgur,
        navigationOptions: {
            title: 'Imgur'
        }
    },
    Index: {
        screen: Index,
        navigationOptions: {
            header: null
        }
    }
});

export default LoginStackNavigator