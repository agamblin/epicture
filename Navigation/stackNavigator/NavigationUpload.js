import {createStackNavigator} from "react-navigation";
import  Upload from '../../Components/Upload'
import UploadImage from "../../Components/UploadImage";

const uploadStackNavigator = createStackNavigator({
    upload: {
        screen: Upload,
        navigationOptions: {
            title: 'Upload'
        }
    },
    uploadImage: {
        screen: UploadImage,
        navigationOptions: {
            title: 'Upload'
        }
    }
});

export default uploadStackNavigator