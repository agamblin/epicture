import React from "react";
import {createBottomTabNavigator} from "react-navigation";
import FeedStackNavigator from './stackNavigator/NavigationFeed'
import uploadStackNavigator from './stackNavigator/NavigationUpload'
import MyImagesTabNavigator from './topNavigator/myImagesTabNavigator'

import { Icon } from 'react-native-elements'
import Search from "../Components/Search";

const ImgurTabNavigator = createBottomTabNavigator({
        Feed: {
            screen: FeedStackNavigator,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Icon
                        name='home'
                        color='black'
                    />
                }
            }
        },
        Search: {
            screen: Search,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Icon
                        name='search'
                        type='feather'
                        color='black'
                    />
                }
            }
            },
        MyImages: {
            screen: MyImagesTabNavigator,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Icon
                        name='picture-o'
                        type='font-awesome'
                        color='black'
                    />
                }
            }
        },
        Upload: {
            screen: uploadStackNavigator,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Icon
                        name='upload'
                        type='feather'
                        color='black'
                    />
                }
            }
        },
},
    {
        tabBarOptions: {
            activeBackgroundColor: '#DDDDDD',
            inactiveBackgroundColor: '#FFFFFF',
            showLabel: false,
            showIcon: true
        }
    }
);

export default ImgurTabNavigator