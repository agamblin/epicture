import { createMaterialTopTabNavigator } from "react-navigation"
import myImagesStackNavigator from '../stackNavigator/NavigationMyImagesScreen'
import favorisStackNavigator from '../stackNavigator/NavigationFavoris'
import Favorites from '../../Components/Favorites'
import { Icon } from "react-native-elements";
import React from "react";

const MyImagesTabNavigator = createMaterialTopTabNavigator({
        Favorites: {
            screen: favorisStackNavigator,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Icon
                        name='heart'
                        type='feather'
                        color='black'
                    />
                }
            }
        },
        Uploaded: {
            screen: myImagesStackNavigator,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Icon
                        name='upload'
                        type='feather'
                        color='black'
                    />
                }
            }
        }
    },
    {
        tabBarOptions: {
            showLabel: false,
            showIcon: true,
            style: {
                backgroundColor: 'white'
            }
        }
    }
    );

export default MyImagesTabNavigator